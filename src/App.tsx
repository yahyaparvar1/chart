import React from "react";
import "./App.css";
import MyPieChart from "./charts/pie";
import RadarChart from "./charts/radar";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <RadarChart />
        <MyPieChart />
      </header>
    </div>
  );
}

export default App;
