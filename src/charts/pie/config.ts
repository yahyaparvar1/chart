export const pieOptions = {
  rotation: -0.5 * Math.PI - (35 / 180) * Math.PI,
  scales: {
    gridLines: {
      display: false,
    },
  },
  title: {
    display: false,
  },
  // responsive: true,
  legend: {
    display: false,
  },
  plugins: {
    datalabels: {
      font: {
        size: 300,
      },
    },
  },

  tooltips: {
    bodyFontSize: 20,
    callbacks: {
      beforeLabel: function (tooltipItem: any, data: any) {
        return `Here is the: ${data.labels[tooltipItem.index]}`;
      },
      label: function (tooltipItem: any, data: any) {
        return ``;
      },
    },
  },
};

const dataSet = [
  {
    label: "VALUE",
    value: 10,
  },
  {
    label: "FUTURE",
    value: 10,
  },
  {
    label: "PAST",
    value: 10,
  },
  {
    label: "HEALTH",
    value: 10,
  },
  {
    label: "DEVIDEND",
    value: 10,
  },
];
export const pieData = {
  labels: dataSet.map((item) => item.label),
  datasets: [
    {
      label: "Arm Sales",
      data: dataSet.map((item) => item.value),
      borderColor: "transparent",
      backgroundColor: Array(dataSet.length).fill("transparent"),
      hoverBackgroundColor: Array(dataSet.length).fill("#ffffff76"),
    },
  ],
};
