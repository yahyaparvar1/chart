import styled from "styled-components";
import { Pie } from "react-chartjs-2";
import { pieData, pieOptions } from "./config";

const MyPieChart = () => {
  return (
    <Wrapper>
      <Pie data={pieData} options={pieOptions}></Pie>
    </Wrapper>
  );
};
const Wrapper = styled.div`
  height: 223px;
  width: 622px;
  margin-top: -239px;
  z-index: 3;
  position: relative;
`;
export default MyPieChart;
