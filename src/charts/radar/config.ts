const gridlinesWidth = [20, 21, 0, 0, 23, 0, 0, 23];

export const radarOptions: Chart.ChartOptions = {
  legend: {
    display: false,
  },

  scale: {
    ticks: {
      stepSize: 1,
      display: false,
      beginAtZero: true,
      max: 10,
    },
    angleLines: {
      display: true,
      lineWidth: 6,
      color: "#424B59",
    },
    gridLines: {
      circular: true,
      color: "#424B59",
      lineWidth: gridlinesWidth,
    },

    pointLabels: {
      fontStyle: "normal",
      fontSize: 16,
      fontColor: "#fff",
    },
  },
  elements: {
    point: {
      radius: 0,
    },
  },
};
