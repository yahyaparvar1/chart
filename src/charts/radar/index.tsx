import styled from "styled-components";
import { Radar } from "react-chartjs-2";
import { CoverCircle, Label, LabelsContainer } from "./styles";
import { radarOptions } from "./config";

const RadarChart = ({
  chartFillColour = "rgba(233, 253, 13, 0.603)",
  chartBorderColour = "rgba(233, 253, 13, 1)",
}) => {
  const chartData = [4, 5, 7, 8.5, 7];
  const data = {
    //These strings ate empty in case we want to use them in the future
    labels: Array(chartData.length).fill(""),
    datasets: [
      {
        data: chartData,
        backgroundColor: chartFillColour,
        borderColor: chartBorderColour,
        lineTension: 0.5,
      },
    ],
  };

  return (
    <Wrapper>
      <LabelsContainer>
        <Label>VALUE</Label>
        <Label>FUTURE</Label>
        <Label>PAST</Label>
        <Label>HEALTH</Label>
        <Label>DEVIDEND</Label>
      </LabelsContainer>
      <CoverCircle />
      <Radar data={data} options={radarOptions}></Radar>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  height: 263px;
  width: 677px;
  pointer-events: none;
  z-index: 1;
  position: relative;
`;
export default RadarChart;
