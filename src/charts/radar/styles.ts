import styled from "styled-components";
export const Label = styled.div``;
export const CoverCircle = styled.div`
  width: 310px;
  height: 310px;
  border-radius: 100%;
  background-color: #2d3641;
  position: absolute;
  top: 25px;
  right: 185px;
  z-index: -1;
`;
export const LabelsContainer = styled.div`
  width: 380px;
  height: 380px;
  top: -5px;
  position: absolute !important;
  right: 150px;
  z-index: -1;
  ${Label} {
    font-size: 18px;

    width: fit-content !important;
    font-family: Arial, Helvetica, sans-serif;
  }
  ${Label}:nth-child(1) {
    position: absolute;
    right: 162px;
  }
  ${Label}:nth-child(2) {
    right: -10px;
    top: 120px;
    transform: rotate(70deg);
    position: absolute;
  }
  ${Label}:nth-child(3) {
    right: 60px;
    top: 315px;
    transform: rotate(-40deg);
    position: absolute;
  }
  ${Label}:nth-child(4) {
    left: 55px;
    top: 315px;
    transform: rotate(40deg);
    position: absolute;
  }
  ${Label}:nth-child(5) {
    position: absolute;
    top: 120px;
    left: -20px;
    transform: rotate(-70deg);
  }
`;
